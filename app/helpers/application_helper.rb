module ApplicationHelper
  def bootstrap_class_for(flash_type)
    case flash_type.to_sym
    when :success
      "alert-success"
    when :error
      "alert-danger"
    when :alert
      "alert-warning"
    when :notice
      "alert-info"
    else
      "alert-#{flash_type}"
    end
  end

  def user_signed_in?
    current_user.present?
  end

  # ==== Examples
  # glyph(:share_alt)
  # # => <i class="icon-share-alt"></i>
  # glyph(:lock, :white)
  # # => <i class="icon-lock icon-white"></i>

  def glyph(*names)
    content_tag :i, nil, class: names.map { |name| "icon-#{name.to_s.gsub("_", "-")}" }
  end
end
