class CancelSchedule
  include Interactor

  def call
    if context.schedule.cancelable?
      context.schedule.destroy
    else
      context.fail!(message: I18n.t("errors.schedule.not_cancelable", duration: context.schedule.cancel_duration))
    end
  end
end
