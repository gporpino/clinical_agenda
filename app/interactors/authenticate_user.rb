class AuthenticateUser
  include Interactor

  def call
    if user = User.authenticate(context.email, context.password)
      context.user = user
    else
      context.fail!(message: I18n.t("errors.password.not_matched"))
    end
  end
end
