class CreateSchedule
  include Interactor

  def call
    context.schedule = Schedule.new(context.params)
    context.schedule.client = context.client
    schedules = Schedule.where(client: context.client)

    schedule_duration = schedules.map(&:session_duration).inject(0, :+)

    total_duration = schedule_duration + context.schedule.session_duration.to_i

    if total_duration <= 1.hour
      context.schedule.save!
    else
      context.fail!(message: I18n.t("errors.schedule.cant_schedule"))
    end
  rescue => e
    context.fail!(error: e.message)
  end
end
