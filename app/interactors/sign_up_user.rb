class SignUpUser
  include Interactor

  def call
    context.user = User.new(context.params)

    context.user.save!
  rescue
    context.fail!
  end
end
