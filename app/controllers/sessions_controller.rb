class SessionsController < ApplicationController
  skip_before_action :authenticate_user!
  # GET /users/sign_in
  def new
    @user = User.new
  end

  # POST /users/sign_in
  def create
    result = AuthenticateUser.call(session_params)
    if result.success?
      session.clear
      self.current_user = result.user
      redirect_to root_path
    else
      @user = User.new
      flash.now[:error] = result.message
      render :new
    end
  end

  def destroy
    session.clear
    redirect_to sign_in_path
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def session_params
    params.require(:user).permit(:email, :password)
  end
end
