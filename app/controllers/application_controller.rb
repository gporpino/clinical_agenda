class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  helper_method :current_user

  def current_user
    if session[:user_id]
      User.find { |u| u.id == session[:user_id] }
    else
      nil
    end
  end

  def current_user=(user)
    session[:user_id] = user.id if user.present?
  end

  def authenticate_user!
    unless @skip_authentication
      redirect_to sign_in_path unless current_user
    end
  end
end
