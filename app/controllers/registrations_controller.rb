class RegistrationsController < ApplicationController
  skip_before_action :authenticate_user!
  # GET /users/sign_up
  def new
    @user = User.new
    @user.build_client
  end

  # GET /users/profile
  def edit
    @user = self.current_user
  end

  # POST /users/sign_up
  def create
    result = SignUpUser.call(params: user_params)
    if result.success?
      self.current_user = result.user
      redirect_to root_path
    else
      @user = result.user
      render :new
    end
  end

  def update
    @user = self.current_user

    if @user.client.update(client_params)
      redirect_to profile_path, notice: "Profile was successfully updated."
    else
      render :edit
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :client_attributes => [:name, :surname, :document, :cellphone])
  end

  def client_params
    params.require(:user).require(:client_attributes).permit(:name, :surname, :document, :cellphone, :address)
  end
end
