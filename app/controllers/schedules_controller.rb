class SchedulesController < ApplicationController
  load_and_authorize_resource
  before_action :set_schedule, only: [:show, :edit, :update, :destroy]

  # GET /schedules
  # GET /schedules.json
  def index
    @schedules = Schedule.all
  end

  # GET /schedules/1
  # GET /schedules/1.json
  def show
  end

  # GET /schedules/new
  def new
    @schedule = Schedule.new
  end

  # GET /schedules/1/edit
  def edit
  end

  # POST /schedules
  # POST /schedules.json
  def create
    result = CreateSchedule.call(params: schedule_params, client: current_user.client)
    @schedule = result.schedule

    if result.success?
      redirect_to @schedule, notice: "Schedule was successfully created."
    else
      flash.now[:error] = result.message if result.message.present?
      # flash.now[:error] = result.error if result.error.present?
      render :new
    end
  end

  # PATCH/PUT /schedules/1
  # PATCH/PUT /schedules/1.json
  def update
    respond_to do |format|
      if @schedule.update(schedule_params)
        format.html { redirect_to @schedule, notice: "Schedule was successfully updated." }
        format.json { render :show, status: :ok, location: @schedule }
      else
        format.html { render :edit }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schedules/1
  # DELETE /schedules/1.json
  def destroy
    result = CancelSchedule.call(schedule: @schedule)

    if result.success?
      redirect_to schedules_url, notice: "Schedule was successfully destroyed."
    else
      redirect_to schedules_url, :flash => {:error => result.message}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_schedule
    @schedule = Schedule.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def schedule_params
    params.require(:schedule).permit(:start_time, :session_duration, :service_id, :client_id)
  end
end
