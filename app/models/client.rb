class Client < ApplicationRecord
  has_one :user

  validates :name, :document, :cellphone, presence: true
end
