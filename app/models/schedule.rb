class Schedule < ApplicationRecord
  attribute :session_duration, :duration

  belongs_to :service
  belongs_to :client

  validates :start_time, :session_duration, presence: true
  validate :validate_start_time_minimum_time,
    :validate_start_time_cannot_be_in_the_past,
    :validate_end_time_maximum_time, if: :start_time

  def end_time
    if start_time and session_duration.present?
      start_time + session_duration
    else
      start_time
    end
  rescue
    nil
  end

  def validate_start_time_minimum_time
    min_hour = 8

    errors.add :start_time, I18n.t("errors.schedule.wrong_range.minimum", :min_hour => min_hour) if start_time.hour < min_hour
  end

  def validate_end_time_maximum_time
    max_hour = 16
    errors.add :session_duration, I18n.t("errors.schedule.wrong_range.maximum", :max_hour => max_hour) if end_time.hour >= max_hour and end_time.min != 0
  end

  def validate_start_time_cannot_be_in_the_past
    if start_time.present? && start_time < Date.today
      errors.add(:start_time, I18n.t("errors.schedule.not_in_past"))
    end
  end

  def cancel_duration
    service.kind.massage? ? 1.day : 2.days
  end

  def cancelable?
    start_time.present? && start_time >= DateTime.now + cancel_duration
  end

  def editable?
    self.cancelable?
  end
end
