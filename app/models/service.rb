class Service < ApplicationRecord
  has_many :schedules

  validates :name, :kind, presence: true

  extend Enumerize
  enumerize :kind, in: [:massage, :treatment]
end
