class User < ApplicationRecord
  attr_accessor :password, :password_confirmation

  belongs_to :client

  accepts_nested_attributes_for :client

  validates :email, :password, :password_confirmation, presence: true
  validates :email, uniqueness: true
  validate :validate_password_confirmation

  before_save :set_password_hash

  def self.hash_password(password)
    BCrypt::Password.create(password).to_s
  end

  def self.test_password(password, hash)
    BCrypt::Password.new(hash) == password
  end

  def self.authenticate(email, password)
    user = User.find { |u| u.email == email }
    if user and User.test_password(password, user.password_hash)
      user
    else
      nil
    end
  end

  private

  def set_password_hash
    self.password_hash = User.hash_password(self.password)
  end

  def validate_password_confirmation
    if password.present?
      errors.add(:password_confirmation, I18n.t("errors.password_confirmation.not_matched")) unless password == password_confirmation
    end
  end
end
