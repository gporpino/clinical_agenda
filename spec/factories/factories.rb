FactoryBot.define do
  factory :user do
    email { "test@test.com" }
    password { "123456" }
  end

  factory :schedule do
    start_time { Time.zone.now }
    session_duration { 30.minutes }
    client { Client.new }
    service { FactoryBot.create :service }
  end

  factory :service do
    sequence(:name)
    kind { :massage }
  end

  # # This will use the User class (Admin would have been guessed)
  # factory :admin, class: User do
  #   first_name { "Admin" }
  #   last_name { "User" }
  #   admin { true }
  # end
end
