require "rails_helper"

describe Schedule do
  subject(:schedule) { FactoryBot.create :schedule }

  before do
    Timecop.freeze(DateTime.new(2010, 1, 1, 15, 0, 0))
  end

  after do
    Timecop.return
  end

  context "is valid" do
    it "with valid attributes" do
      expect(schedule).to be_valid
    end

    it "with a start_time at 8h" do
      schedule.start_time = Time.zone.now.change({hour: 8})
      expect(schedule).to be_valid
    end

    it "with a end_time at 16h" do
      schedule.start_time = Time.zone.now.change({hour: 15, min: 30})
      expect(schedule).to be_valid
    end
  end

  context "is not valid" do
    it "without a client" do
      schedule.client = nil
      expect(schedule).to_not be_valid
    end

    it "without a service" do
      schedule.service = nil
      expect(schedule).to_not be_valid
    end

    it "without a start_time" do
      schedule.start_time = nil
      expect(schedule).to_not be_valid
    end

    it "with a end_time after 16h" do
      schedule.start_time = Time.zone.now.change({hour: 17})
      expect(schedule).to_not be_valid
    end

    it "with a start_time before 8h" do
      schedule.start_time = Time.zone.now.change({hour: 7})
      expect(schedule).to_not be_valid
    end

    it "with a start_time in the past" do
      schedule.start_time = Time.zone.now.beginning_of_day
      expect(schedule).to_not be_valid
    end
  end

  context "when service type is massage" do
    it "is cancelable with a start_time about 1 day" do
      schedule.start_time = DateTime.now + 1.day
      expect(schedule).to be_cancelable
    end

    it "is cancelable with a start_time about more then 1 day" do
      schedule.start_time = DateTime.now + 2.days
      expect(schedule).to be_cancelable
    end

    it "is not cancelable with a start_time equal now" do
      expect(schedule).to_not be_cancelable
    end

    it "is not cancelable with a start_time less then 1 day" do
      schedule.start_time = DateTime.now + 23.hours + 59.minutes
      expect(schedule).to_not be_cancelable
    end

    it "is not cancelable with a start_time on past" do
      schedule.start_time = DateTime.now.yesterday
      expect(schedule).to_not be_cancelable
    end
  end

  context "when service type is not massage" do
    before { schedule.service.kind = :treatment }

    it "is cancelable with a start_time about 2 day" do
      schedule.start_time = DateTime.now + 2.day
      expect(schedule).to be_cancelable
    end

    it "is cancelable with a start_time about more then 2 day" do
      schedule.start_time = DateTime.now + 3.days
      expect(schedule).to be_cancelable
    end

    it "is not cancelable with a start_time equal now" do
      expect(schedule).to_not be_cancelable
    end

    it "is not cancelable with a start_time less then 1 day" do
      schedule.start_time = DateTime.now + 47.hours + 59.minutes
      expect(schedule).to_not be_cancelable
    end

    it "is not cancelable with a start_time on past" do
      schedule.start_time = DateTime.now.yesterday
      expect(schedule).to_not be_cancelable
    end
  end
end
