class AddAddressToClient < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :address, :text
  end
end
