class AddClientToSchedules < ActiveRecord::Migration[5.2]
  def change
    add_reference :schedules, :client, foreign_key: true
  end
end
