# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Service.create(name: "Massagem Linfática", kind: :massage)
Service.create(name: "Massagem Bowen", kind: :massage)
Service.create(name: "Tratamento Facial", kind: :treatment)
Service.create(name: "Tratamento Neuromuscular", kind: :treatment)