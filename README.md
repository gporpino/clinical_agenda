# README

### to run the project follow the steps:

run:

> bundle install

> rake db:migrate db:seed

> rails s

sign_up at: /auth/users/sign_up

### Requirements:

to run this project you must install rails 5.2

### Test the Project:

run:

> bundle exec rspec

### The Project:

- MVC: This project uses the MVC pattern concepts. Rails implements MVC as "pure" concept. Diferent to others platforms the Model is the main diference. THe model is the repesentation of domain and database.

- DRY: This project uses the dont repeat your self, to simplify the code was used the Interactor pattern that is on service layer and is responsible to bridge the controller to models. With this it is possible to test only the integration without dependency.

- BDD: This project uses BDD to test. BDD focus on behaviour of application to certify that the code are doing what the client wants.

Production:
This project is runnig at https://clinicalagenda.herokuapp.com/
