Rails.application.routes.draw do
  resources :clients
  resources :schedules
  resources :services
  resources :users

  scope :auth do
    # resource :sign_in, only: [:new, :create, :destroy], controller: :sessions
    scope :users, controller: :sessions do
      get :sign_in, action: :new
      post :sign_in, action: :create
      patch :sign_in, action: :create
      delete :sign_in, action: :destroy
    end

    scope :users, controller: :registrations do
      get :sign_up, action: :new
      post :sign_up, action: :create
      patch :sign_up, action: :create

      get :profile, action: :edit
      patch :profile, action: :update
    end
    # resource :sign_up, only: [:new, :create], controller: :registrations
  end

  root to: "home#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
